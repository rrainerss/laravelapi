<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Thing;

class ApiController extends Controller
{
    //
    function getData()
    {
        return Thing::all();
    }

    function postData(Request $req)
    {
        $thing = new Thing;
        $thing->title=$req->title;
        $thing->id=$req->id;
        $thing->text=$req->text;
        $thing->save();
    }

    function updateData(Request $req)
    {
        $thing = new Thing;
        $thing = Thing::find($req->id);
        $thing->title=$req->title;
        $thing->id=$req->id;
        $thing->text=$req->text;
        $thing->save();
    }

    function deleteData(Request $req)
    {
        $thing = new Thing;
        $thing = Thing::find($req->id);
        if($thing)
            $thing->delete(); 
    }
}
